from windows import WindowMessage, WindowReadFile, WindowSaveFile
from os import path, mkdir, chdir


class Tab3:
    def __init__(self, nethandler):
        self.WindowRead = WindowReadFile()
        self.WindowSave = WindowSaveFile()
        self.nethandler = nethandler

    def saveconfig(self):
        command = "write"
        self.window_messages("Settings router have been saved")
        self.nethandler.enable()
        self.nethandler.send_command(command)

    def read_config_from_file(self):
        def create_directory():
            if not path.isdir('save_config'):
                chdir(".")
                mkdir('save_config')

        create_directory()
        self.WindowRead.show()
        self.WindowRead.browse.clicked.connect(lambda: self.WindowRead.browser_file(self.nethandler))

    def save_config_to_file(self):
        def create_directory():
            if not path.isdir('save_config'):
                chdir(".")
                mkdir('save_config')

        create_directory()
        command = "show running-config"
        output = self.nethandler.send_command(command)
        w = output.replace('!', '')
        temp = [s.strip() for s in w.split("\n") if s.strip() != ""][4:]
        config = '\n'.join(temp)
        self.WindowSave.show()
        self.WindowSave.but_save.clicked.connect(lambda: self.WindowSave.save_file(config))

    def window_messages(self, message):
        self.Window_m = WindowMessage(message)
        self.Window_m.show()
