from windows import WindowMessage


class Tab2:
    def __init__(self, nethandler):
        self.nethandler = nethandler

    def setport(self, portstatus_ischecked, interface):
        if portstatus_ischecked:
            command = [
                f"int {interface}"
                "no shutdown"
            ]
            self.nethandler.send_config_set(command)
        else:
            command = [
                f"int {interface}",
                "shutdown"
            ]
            self.nethandler.send_config_set(command)

    def add_loopback(self):
        def count_loopback():
            command = "show ip int brief | include Loopback"
            self.nethandler.enable()
            output = self.nethandler.send_command(command)
            return len([w for w in output.split() if w.startswith("Loop")])

        num_loopback = count_loopback()
        command = "int Loopback{}".format(num_loopback + 1)
        self.nethandler.send_config_set(command)

    def address_conf_ipv4(self, ip_address, mask, interface):
        if ip_address and mask:
            try:
                command = [
                    f"int {interface}",
                    f"ip address {ip_address} {mask}"
                ]
                self.nethandler.send_config_set(command)
            except Exception as err:
                exception_type = type(err).__name__
                self.windows_message(f"The operation was unsuccessful: {exception_type}")
        else:
            self.windows_message("not all fields in the ipv4 section have been completed ")

    def address_conf_ipv6(self, ipv6_address, mask, interface):
        if ipv6_address and mask:
            try:
                command = [
                    f"int {interface}",
                    "ipv6 address {0} {1}".format(ipv6_address, mask)
                ]
                self.nethandler.send_config_set(command)
            except Exception as err:
                exception_type = type(err).__name__
                self.windows_message(f"The operation was unsuccessful: {exception_type}")
        else:
            self.windows_message("not all fields in the ipv6 section have been completed ")

    def subinterfaces(self, sub_number, vlan, ip_address, mask, interface):
        if sub_number and vlan and ip_address and mask:
            try:
                command = [
                    "int {}.{}".format(interface, int(sub_number)),
                    "encapsulation dot1Q {}".format(int(vlan)),
                    "ip address {} {}".format(ip_address, mask)
                ]
                self.nethandler.send_config_set(command)
            except ValueError:
                self.windows_message("An invalid type value was supplied")
            except Exception as err:
                exception_type = type(err).__name__
                self.windows_message(f"The operation was unsuccessful: {exception_type}")

    def windows_message(self, text):
        self.WindowM = WindowMessage(text)
        self.WindowM.show()

