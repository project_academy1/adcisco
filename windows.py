from os import chdir, path
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QFileDialog


class WindowMessage(QWidget):
    def __init__(self, message):
        super().__init__()
        layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel(message)
        layout.addWidget(self.label)
        self.setLayout(layout)
        self.setFixedSize(300, 50)
        self.setWindowTitle("Message")
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


class WindowSaveFile(QWidget):
    def __init__(self):
        super().__init__()
        layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel("Filename")
        self.entry = QtWidgets.QLineEdit()
        self.but_save = QtWidgets.QPushButton("save file")
        layout.addWidget(self.label)
        layout.addWidget(self.entry)
        layout.addWidget(self.but_save)
        self.setLayout(layout)
        self.setFixedSize(300, 100)
        self.setWindowTitle("Window Save File ")
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def save_file(self, line):
        self.close()
        filename = self.entry.text()
        chdir('save_config')
        try:
            with open('{}.ios'.format(filename), 'w') as writer:
                writer.write(line)
        except IOError:
            self.windows_message("Failed to  save the file")
        self.windows_message("the configuration saved to a file")
        chdir("..")


class WindowReadFile(QWidget):
    def __init__(self):
        super().__init__()
        layout_vertical = QtWidgets.QVBoxLayout()
        layout_h = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel("Filename")
        self.entry = QtWidgets.QLineEdit()
        self.browse = QtWidgets.QPushButton("Browse")
        self.btn_read = QtWidgets.QPushButton("read file")
        layout_vertical.addWidget(self.label)
        layout_h.addWidget(self.entry)
        layout_h.addWidget(self.browse)
        layout_vertical.addLayout(layout_h)
        layout_vertical.addWidget(self.btn_read)
        self.setLayout(layout_vertical)
        self.setFixedSize(300, 100)
        self.setWindowTitle("Window Read File")
        self.entry.setReadOnly(True)
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def browser_file(self, nethandler):

        fname = QFileDialog.getOpenFileName(
            parent=self,
            options=QtWidgets.QFileDialog.DontUseNativeDialog,
            caption='Select a data file',
            directory='save_config',
            filter='IOS files(*.ios)',
            initialFilter='IOS files(*.ios)'
        )
        path_file = fname[0]
        self.entry.setReadOnly(False)
        self.entry.setText(path.basename(path_file))
        self.entry.setReadOnly(True)
        self.btn_read.clicked.connect(lambda: self.read_file(path_file, nethandler))

    def read_file(self, path_file, nethandler):
        try:
            with open(path_file, 'r') as reader:
                data = reader.read()
        except IOError:
            self.windows_message("Failed to read  the file")
        self.windows_message("The file was read ")
        self.close()
        command_conf = [x for x in data.split("\n")]
        try:
            nethandler.send_config_set(command_conf)
        except Exception as err:
            exception_type = type(err).__name__
            self.windows_message(f"The operation was unsuccessful: {exception_type}")
        self.windows_message("The operation was successful")


def windows_message(self, text):
    self.WindowM = WindowMessage(text)
    self.WindowM.show()
