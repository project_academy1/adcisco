import pickle
from PyQt5 import QtCore, QtWidgets, QtGui
from netmiko import ConnectHandler
import textfsm, os
from windows import WindowMessage
from tab3_functions import Tab3
from tab2_functions import Tab2
from tab4_functions import Tab4


class Ui_ripRoutingPassiveInterfacesListWidget(object):
    def setupUi(self, ripRoutingPassiveInterfacesListWidget):
        ripRoutingPassiveInterfacesListWidget.setObjectName("ripRoutingPassiveInterfacesListWidget")
        ripRoutingPassiveInterfacesListWidget.resize(762, 554)
        self.tabWidget = QtWidgets.QTabWidget(ripRoutingPassiveInterfacesListWidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 761, 551))
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.IPv4radioButton = QtWidgets.QRadioButton(self.tab)
        self.IPv4radioButton.setGeometry(QtCore.QRect(0, 0, 115, 21))
        self.IPv4radioButton.setObjectName("IPv4radioButton")
        self.IPv6radioButton = QtWidgets.QRadioButton(self.tab)
        self.IPv6radioButton.setGeometry(QtCore.QRect(120, 0, 115, 21))
        self.IPv6radioButton.setObjectName("IPv6radioButton")
        self.toolBox = QtWidgets.QToolBox(self.tab)
        self.toolBox.setGeometry(QtCore.QRect(0, 30, 751, 261))
        self.toolBox.setObjectName("toolBox")
        self.page = QtWidgets.QWidget()
        self.page.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.page.setObjectName("page")
        self.runConfTextEdit = QtWidgets.QTextEdit(self.page)
        self.runConfTextEdit.setGeometry(QtCore.QRect(0, 0, 761, 87))
        self.runConfTextEdit.setObjectName("runConfTextEdit")
        self.toolBox.addItem(self.page, "")
        self.page_3 = QtWidgets.QWidget()
        self.page_3.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.page_3.setObjectName("page_3")
        self.interfaceBriefTextEdit = QtWidgets.QTextEdit(self.page_3)
        self.interfaceBriefTextEdit.setGeometry(QtCore.QRect(0, 0, 781, 87))
        self.interfaceBriefTextEdit.setObjectName("interfaceBriefTextEdit")
        self.toolBox.addItem(self.page_3, "")
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.page_2.setObjectName("page_2")
        self.interfacesTextEdit = QtWidgets.QTextEdit(self.page_2)
        self.interfacesTextEdit.setGeometry(QtCore.QRect(0, 0, 771, 131))
        self.interfacesTextEdit.setObjectName("interfacesTextEdit")
        self.toolBox.addItem(self.page_2, "")
        self.page_4 = QtWidgets.QWidget()
        self.page_4.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.page_4.setObjectName("page_4")
        self.showACLTextEdit = QtWidgets.QTextEdit(self.page_4)
        self.showACLTextEdit.setGeometry(QtCore.QRect(-10, -10, 761, 87))
        self.showACLTextEdit.setObjectName("showACLTextEdit")
        self.toolBox.addItem(self.page_4, "")
        self.page_5 = QtWidgets.QWidget()
        self.page_5.setGeometry(QtCore.QRect(0, 0, 751, 126))
        self.page_5.setObjectName("page_5")
        self.ipRouteTextEdit = QtWidgets.QTextEdit(self.page_5)
        self.ipRouteTextEdit.setGeometry(QtCore.QRect(0, 0, 781, 87))
        self.ipRouteTextEdit.setObjectName("ipRouteTextEdit")
        self.toolBox.addItem(self.page_5, "")
        self.tabWidget.addTab(self.tab, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.tab_3)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 20, 721, 411))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.addLoopbuttn = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.addLoopbuttn.setObjectName("addLoopbuttn")
        self.gridLayout.addWidget(self.addLoopbuttn, 0, 3, 1, 1)
        self.duplexLabel = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.duplexLabel.setObjectName("duplexLabel")
        self.gridLayout.addWidget(self.duplexLabel, 1, 0, 1, 1)
        self.halfduplexRadio = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.halfduplexRadio.setObjectName("halfduplexRadio")
        self.gridLayout.addWidget(self.halfduplexRadio, 1, 1, 1, 1)
        self.autoDuplexRadio = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.autoDuplexRadio.setObjectName("autoDuplexRadio")
        self.gridLayout.addWidget(self.autoDuplexRadio, 1, 3, 1, 1)
        self.portLabel = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.portLabel.setObjectName("portLabel")
        self.gridLayout.addWidget(self.portLabel, 0, 0, 1, 1)
        self.portStatus = QtWidgets.QCheckBox(self.verticalLayoutWidget)
        self.portStatus.setObjectName("portStatus")
        self.gridLayout.addWidget(self.portStatus, 0, 1, 1, 1)
        self.fullDuplexRadio = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.fullDuplexRadio.setObjectName("fullDuplexRadio")
        self.gridLayout.addWidget(self.fullDuplexRadio, 1, 2, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        self.interfaceComboBox = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.interfaceComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)
        self.interfaceComboBox.setObjectName("interfaceComboBox")
        self.horizontalLayout.addWidget(self.interfaceComboBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.interfaceToolBox = QtWidgets.QToolBox(self.verticalLayoutWidget)
        self.interfaceToolBox.setObjectName("interfaceToolBox")
        self.page_9 = QtWidgets.QWidget()
        self.page_9.setGeometry(QtCore.QRect(0, 0, 719, 272))
        self.page_9.setObjectName("page_9")
        self.verticalLayoutWidget_2 = QtWidgets.QWidget(self.page_9)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(10, -1, 691, 291))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.groupBox_5 = QtWidgets.QGroupBox(self.verticalLayoutWidget_2)
        self.groupBox_5.setObjectName("groupBox_5")
        self.gridLayoutWidget_2 = QtWidgets.QWidget(self.groupBox_5)
        self.gridLayoutWidget_2.setGeometry(QtCore.QRect(4, 10, 651, 121))
        self.gridLayoutWidget_2.setObjectName("gridLayoutWidget_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_8 = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.label_8.setObjectName("label_8")
        self.gridLayout_2.addWidget(self.label_8, 0, 0, 1, 1)
        self.intIPv4SubnetMask = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.intIPv4SubnetMask.setObjectName("intIPv4SubnetMask")
        self.gridLayout_2.addWidget(self.intIPv4SubnetMask, 1, 1, 1, 1)
        self.intIPv4Address = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.intIPv4Address.setObjectName("intIPv4Address")
        self.gridLayout_2.addWidget(self.intIPv4Address, 0, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.label_5.setObjectName("label_5")
        self.gridLayout_2.addWidget(self.label_5, 1, 0, 1, 1)
        self.intIPv4buttonBox = QtWidgets.QDialogButtonBox(self.gridLayoutWidget_2)
        self.intIPv4buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.intIPv4buttonBox.setObjectName("intIPv4buttonBox")
        self.gridLayout_2.addWidget(self.intIPv4buttonBox, 2, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 2, 0, 1, 1)
        self.verticalLayout_3.addWidget(self.groupBox_5)
        self.groupBox_6 = QtWidgets.QGroupBox(self.verticalLayoutWidget_2)
        self.groupBox_6.setObjectName("groupBox_6")
        self.gridLayoutWidget_3 = QtWidgets.QWidget(self.groupBox_6)
        self.gridLayoutWidget_3.setGeometry(QtCore.QRect(0, 10, 651, 109))
        self.gridLayoutWidget_3.setObjectName("gridLayoutWidget_3")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.gridLayoutWidget_3)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.intIPv6buttonBox = QtWidgets.QDialogButtonBox(self.gridLayoutWidget_3)
        self.intIPv6buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.intIPv6buttonBox.setObjectName("intIPv6buttonBox")
        self.gridLayout_3.addWidget(self.intIPv6buttonBox, 2, 1, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.gridLayoutWidget_3)
        self.label_6.setObjectName("label_6")
        self.gridLayout_3.addWidget(self.label_6, 1, 0, 1, 1)
        self.intIPv6Address = QtWidgets.QLineEdit(self.gridLayoutWidget_3)
        self.intIPv6Address.setObjectName("intIPv6Address")
        self.gridLayout_3.addWidget(self.intIPv6Address, 0, 1, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.gridLayoutWidget_3)
        self.label_12.setObjectName("label_12")
        self.gridLayout_3.addWidget(self.label_12, 0, 0, 1, 1)
        self.intIPv6SubnetMask = QtWidgets.QLineEdit(self.gridLayoutWidget_3)
        self.intIPv6SubnetMask.setObjectName("intIPv6SubnetMask")
        self.gridLayout_3.addWidget(self.intIPv6SubnetMask, 1, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem1, 2, 0, 1, 1)
        self.verticalLayout_3.addWidget(self.groupBox_6)
        self.interfaceToolBox.addItem(self.page_9, "")
        self.page_10 = QtWidgets.QWidget()
        self.page_10.setGeometry(QtCore.QRect(0, 0, 719, 272))
        self.page_10.setObjectName("page_10")
        self.gridLayoutWidget_4 = QtWidgets.QWidget(self.page_10)
        self.gridLayoutWidget_4.setGeometry(QtCore.QRect(20, 10, 671, 241))
        self.gridLayoutWidget_4.setObjectName("gridLayoutWidget_4")
        self.formLayout = QtWidgets.QFormLayout(self.gridLayoutWidget_4)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.gridLayoutWidget_4)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.subinterfaceNumberLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_4)
        self.subinterfaceNumberLineEdit.setObjectName("subinterfaceNumberLineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.subinterfaceNumberLineEdit)
        self.label_11 = QtWidgets.QLabel(self.gridLayoutWidget_4)
        self.label_11.setObjectName("label_11")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_11)
        self.vlanID = QtWidgets.QLineEdit(self.gridLayoutWidget_4)
        self.vlanID.setObjectName("vlanID")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.vlanID)
        self.label_13 = QtWidgets.QLabel(self.gridLayoutWidget_4)
        self.label_13.setObjectName("label_13")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_13)
        self.subinterfaceAddressLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_4)
        self.subinterfaceAddressLineEdit.setObjectName("subinterfaceAddressLineEdit")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.subinterfaceAddressLineEdit)
        self.label_18 = QtWidgets.QLabel(self.gridLayoutWidget_4)
        self.label_18.setObjectName("label_18")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_18)
        self.subinterfaceMaskLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_4)
        self.subinterfaceMaskLineEdit.setObjectName("subinterfaceMaskLineEdit")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.subinterfaceMaskLineEdit)
        self.configurePushButton_2 = QtWidgets.QDialogButtonBox(self.gridLayoutWidget_4)
        self.configurePushButton_2.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.configurePushButton_2.setObjectName("configurePushButton_2")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.configurePushButton_2)
        self.interfaceToolBox.addItem(self.page_10, "")
        self.page_11 = QtWidgets.QWidget()
        self.page_11.setGeometry(QtCore.QRect(0, 0, 719, 272))
        self.page_11.setObjectName("page_11")
        self.gridLayoutWidget_10 = QtWidgets.QWidget(self.page_11)
        self.gridLayoutWidget_10.setGeometry(QtCore.QRect(9, 9, 691, 151))
        self.gridLayoutWidget_10.setObjectName("gridLayoutWidget_10")
        self.gridLayout_9 = QtWidgets.QGridLayout(self.gridLayoutWidget_10)
        self.gridLayout_9.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.label_39 = QtWidgets.QLabel(self.gridLayoutWidget_10)
        self.label_39.setObjectName("label_39")
        self.gridLayout_9.addWidget(self.label_39, 0, 0, 1, 1)
        self.intAccLIN = QtWidgets.QComboBox(self.gridLayoutWidget_10)
        self.intAccLIN.setObjectName("intAccLIN")
        self.gridLayout_9.addWidget(self.intAccLIN, 0, 1, 1, 1)
        self.label_40 = QtWidgets.QLabel(self.gridLayoutWidget_10)
        self.label_40.setObjectName("label_40")
        self.gridLayout_9.addWidget(self.label_40, 1, 0, 1, 1)
        self.intAccLOUT = QtWidgets.QComboBox(self.gridLayoutWidget_10)
        self.intAccLOUT.setObjectName("intAccLOUT")
        self.gridLayout_9.addWidget(self.intAccLOUT, 1, 1, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.page_11)
        self.pushButton.setGeometry(QtCore.QRect(300, 180, 97, 33))
        self.pushButton.setObjectName("pushButton")
        self.interfaceToolBox.addItem(self.page_11, "")
        self.verticalLayout.addWidget(self.interfaceToolBox)
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.groupBox = QtWidgets.QGroupBox(self.tab_2)
        self.groupBox.setGeometry(QtCore.QRect(0, 0, 761, 431))
        self.groupBox.setObjectName("groupBox")
        self.formLayoutWidget = QtWidgets.QWidget(self.groupBox)
        self.formLayoutWidget.setGeometry(QtCore.QRect(10, 20, 731, 321))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout_3 = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout_3.setContentsMargins(0, 0, 0, 0)
        self.formLayout_3.setObjectName("formLayout_3")
        self.hostnameLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.hostnameLabel.setObjectName("hostnameLabel")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.hostnameLabel)
        self.hostnameLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.hostnameLineEdit.setObjectName("hostnameLineEdit")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.hostnameLineEdit)
        self.secretLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.secretLabel.setObjectName("secretLabel")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.secretLabel)
        self.secretLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.secretLineEdit.setObjectName("secretLineEdit")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.secretLineEdit)
        self.bannerMotdLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.bannerMotdLabel.setObjectName("bannerMotdLabel")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.bannerMotdLabel)
        self.bannerMotdLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.bannerMotdLineEdit.setObjectName("bannerMotdLineEdit")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.bannerMotdLineEdit)
        self.domainlookupCheckBox = QtWidgets.QCheckBox(self.formLayoutWidget)
        self.domainlookupCheckBox.setObjectName("domainlookupCheckBox")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.domainlookupCheckBox)
        self.configurePushButton = QtWidgets.QDialogButtonBox(self.formLayoutWidget)
        self.configurePushButton.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.configurePushButton.setObjectName("configurePushButton")
        self.formLayout_3.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.configurePushButton)
        self.saveRunPushFileButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.saveRunPushFileButton.setObjectName("saveRunPushFileButton")
        self.formLayout_3.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.saveRunPushFileButton)
        self.loadRunPushButton_2 = QtWidgets.QPushButton(self.formLayoutWidget)
        self.loadRunPushButton_2.setObjectName("loadRunPushButton_2")
        self.formLayout_3.setWidget(7, QtWidgets.QFormLayout.FieldRole, self.loadRunPushButton_2)
        self.saveRunPushButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.saveRunPushButton.setObjectName("saveRunPushButton")
        self.formLayout_3.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.saveRunPushButton)
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.toolBox_2 = QtWidgets.QToolBox(self.tab_4)
        self.toolBox_2.setGeometry(QtCore.QRect(10, 12, 731, 501))
        self.toolBox_2.setObjectName("toolBox_2")
        self.page_12 = QtWidgets.QWidget()
        self.page_12.setGeometry(QtCore.QRect(0, 0, 731, 393))
        self.page_12.setObjectName("page_12")
        self.toolBox_3 = QtWidgets.QToolBox(self.page_12)
        self.toolBox_3.setGeometry(QtCore.QRect(0, 0, 721, 391))
        self.toolBox_3.setObjectName("toolBox_3")
        self.page_17 = QtWidgets.QWidget()
        self.page_17.setGeometry(QtCore.QRect(0, 0, 721, 337))
        self.page_17.setObjectName("page_17")
        self.toolBox_4 = QtWidgets.QToolBox(self.page_17)
        self.toolBox_4.setGeometry(QtCore.QRect(0, 0, 711, 331))
        self.toolBox_4.setObjectName("toolBox_4")
        self.page_19 = QtWidgets.QWidget()
        self.page_19.setGeometry(QtCore.QRect(0, 0, 711, 277))
        self.page_19.setObjectName("page_19")
        self.gridLayoutWidget_6 = QtWidgets.QWidget(self.page_19)
        self.gridLayoutWidget_6.setGeometry(QtCore.QRect(0, 0, 701, 271))
        self.gridLayoutWidget_6.setObjectName("gridLayoutWidget_6")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.gridLayoutWidget_6)
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.aclNumStdSourceAddressLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.aclNumStdSourceAddressLineEdit.setObjectName("aclNumStdSourceAddressLineEdit")
        self.gridLayout_4.addWidget(self.aclNumStdSourceAddressLineEdit, 8, 0, 1, 1)
        self.aclNumStdPermitPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_6)
        self.aclNumStdPermitPushButton.setObjectName("aclNumStdPermitPushButton")
        self.gridLayout_4.addWidget(self.aclNumStdPermitPushButton, 9, 0, 1, 1)
        self.label_22 = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.label_22.setObjectName("label_22")
        self.gridLayout_4.addWidget(self.label_22, 0, 0, 1, 1)
        self.aclNumStdNumLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.aclNumStdNumLineEdit.setObjectName("aclNumStdNumLineEdit")
        self.gridLayout_4.addWidget(self.aclNumStdNumLineEdit, 0, 1, 1, 1)
        self.aclNumStdCurrentLineEdit = QtWidgets.QTextEdit(self.gridLayoutWidget_6)
        self.aclNumStdCurrentLineEdit.setObjectName("aclNumStdCurrentLineEdit")
        self.gridLayout_4.addWidget(self.aclNumStdCurrentLineEdit, 2, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.label_4.setObjectName("label_4")
        self.gridLayout_4.addWidget(self.label_4, 2, 0, 1, 1)
        self.aclNumStdDenyPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_6)
        self.aclNumStdDenyPushButton.setObjectName("aclNumStdDenyPushButton")
        self.gridLayout_4.addWidget(self.aclNumStdDenyPushButton, 9, 1, 1, 1)
        self.aclNumStdSourceMaskLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.aclNumStdSourceMaskLineEdit.setObjectName("aclNumStdSourceMaskLineEdit")
        self.gridLayout_4.addWidget(self.aclNumStdSourceMaskLineEdit, 8, 1, 1, 1)
        self.label_17 = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.label_17.setObjectName("label_17")
        self.gridLayout_4.addWidget(self.label_17, 7, 1, 1, 1)
        self.label_15 = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.label_15.setObjectName("label_15")
        self.gridLayout_4.addWidget(self.label_15, 7, 0, 1, 1)
        self.aclNumStdAddPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_6)
        self.aclNumStdAddPushButton.setObjectName("aclNumStdAddPushButton")
        self.gridLayout_4.addWidget(self.aclNumStdAddPushButton, 10, 0, 1, 1)
        self.toolBox_4.addItem(self.page_19, "")
        self.page_20 = QtWidgets.QWidget()
        self.page_20.setGeometry(QtCore.QRect(0, 0, 711, 277))
        self.page_20.setObjectName("page_20")
        self.gridLayoutWidget_7 = QtWidgets.QWidget(self.page_20)
        self.gridLayoutWidget_7.setGeometry(QtCore.QRect(0, 0, 701, 271))
        self.gridLayoutWidget_7.setObjectName("gridLayoutWidget_7")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.gridLayoutWidget_7)
        self.gridLayout_6.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.label_24 = QtWidgets.QLabel(self.gridLayoutWidget_7)
        self.label_24.setObjectName("label_24")
        self.gridLayout_6.addWidget(self.label_24, 2, 0, 1, 1)
        self.aclNumExtDestinationMaskLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_7)
        self.aclNumExtDestinationMaskLineEdit.setObjectName("aclNumExtDestinationMaskLineEdit")
        self.gridLayout_6.addWidget(self.aclNumExtDestinationMaskLineEdit, 10, 1, 1, 1)
        self.label_27 = QtWidgets.QLabel(self.gridLayoutWidget_7)
        self.label_27.setObjectName("label_27")
        self.gridLayout_6.addWidget(self.label_27, 9, 0, 1, 1)
        self.aclNumExtPermitPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_7)
        self.aclNumExtPermitPushButton.setObjectName("aclNumExtPermitPushButton")
        self.gridLayout_6.addWidget(self.aclNumExtPermitPushButton, 11, 0, 1, 1)
        self.label_28 = QtWidgets.QLabel(self.gridLayoutWidget_7)
        self.label_28.setObjectName("label_28")
        self.gridLayout_6.addWidget(self.label_28, 9, 1, 1, 1)
        self.label_25 = QtWidgets.QLabel(self.gridLayoutWidget_7)
        self.label_25.setObjectName("label_25")
        self.gridLayout_6.addWidget(self.label_25, 7, 1, 1, 1)
        self.aclNumExtSourceAddressLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_7)
        self.aclNumExtSourceAddressLineEdit.setObjectName("aclNumExtSourceAddressLineEdit")
        self.gridLayout_6.addWidget(self.aclNumExtSourceAddressLineEdit, 8, 0, 1, 1)
        self.label_26 = QtWidgets.QLabel(self.gridLayoutWidget_7)
        self.label_26.setObjectName("label_26")
        self.gridLayout_6.addWidget(self.label_26, 7, 0, 1, 1)
        self.aclNumExtDenyPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_7)
        self.aclNumExtDenyPushButton.setObjectName("aclNumExtDenyPushButton")
        self.gridLayout_6.addWidget(self.aclNumExtDenyPushButton, 11, 1, 1, 1)
        self.aclNumExtSourceMaskLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_7)
        self.aclNumExtSourceMaskLineEdit.setObjectName("aclNumExtSourceMaskLineEdit")
        self.gridLayout_6.addWidget(self.aclNumExtSourceMaskLineEdit, 8, 1, 1, 1)
        self.label_23 = QtWidgets.QLabel(self.gridLayoutWidget_7)
        self.label_23.setObjectName("label_23")
        self.gridLayout_6.addWidget(self.label_23, 0, 0, 1, 1)
        self.aclNumExtDestinationAddressLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_7)
        self.aclNumExtDestinationAddressLineEdit.setObjectName("aclNumExtDestinationAddressLineEdit")
        self.gridLayout_6.addWidget(self.aclNumExtDestinationAddressLineEdit, 10, 0, 1, 1)
        self.aclNumExtCurrentTextEdit = QtWidgets.QTextEdit(self.gridLayoutWidget_7)
        self.aclNumExtCurrentTextEdit.setObjectName("aclNumExtCurrentTextEdit")
        self.gridLayout_6.addWidget(self.aclNumExtCurrentTextEdit, 2, 1, 1, 1)
        self.aclNumExtNumLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_7)
        self.aclNumExtNumLineEdit.setObjectName("aclNumExtNumLineEdit")
        self.gridLayout_6.addWidget(self.aclNumExtNumLineEdit, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.gridLayoutWidget_7)
        self.label_2.setObjectName("label_2")
        self.gridLayout_6.addWidget(self.label_2, 7, 2, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.gridLayoutWidget_7)
        self.label_3.setObjectName("label_3")
        self.gridLayout_6.addWidget(self.label_3, 9, 2, 1, 1)
        self.aclNumExtSourceProtocolLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_7)
        self.aclNumExtSourceProtocolLineEdit.setObjectName("aclNumExtSourceProtocolLineEdit")
        self.gridLayout_6.addWidget(self.aclNumExtSourceProtocolLineEdit, 8, 2, 1, 1)

        self.aclNumExtAddPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_7)
        self.aclNumExtAddPushButton.setObjectName("aclNumExtAddPushButton")
        self.gridLayout_6.addWidget(self.aclNumExtAddPushButton, 11, 2, 1, 1)
        self.toolBox_4.addItem(self.page_20, "")
        self.toolBox_3.addItem(self.page_17, "")
        self.page_18 = QtWidgets.QWidget()
        self.page_18.setGeometry(QtCore.QRect(0, 0, 721, 337))
        self.page_18.setObjectName("page_18")
        self.toolBox_5 = QtWidgets.QToolBox(self.page_18)
        self.toolBox_5.setGeometry(QtCore.QRect(0, 0, 711, 331))
        self.toolBox_5.setObjectName("toolBox_5")
        self.page_21 = QtWidgets.QWidget()
        self.page_21.setGeometry(QtCore.QRect(0, 0, 711, 277))
        self.page_21.setObjectName("page_21")
        self.gridLayoutWidget_8 = QtWidgets.QWidget(self.page_21)
        self.gridLayoutWidget_8.setGeometry(QtCore.QRect(0, 0, 711, 264))
        self.gridLayoutWidget_8.setObjectName("gridLayoutWidget_8")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.gridLayoutWidget_8)
        self.gridLayout_7.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.aclNameStdSourceAddressLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_8)
        self.aclNameStdSourceAddressLineEdit.setObjectName("aclNameStdSourceAddressLineEdit")
        self.gridLayout_7.addWidget(self.aclNameStdSourceAddressLineEdit, 8, 0, 1, 1)
        self.aclNameStdPermitPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_8)
        self.aclNameStdPermitPushButton.setObjectName("aclNameStdPermitPushButton")
        self.gridLayout_7.addWidget(self.aclNameStdPermitPushButton, 9, 0, 1, 1)
        self.label_29 = QtWidgets.QLabel(self.gridLayoutWidget_8)
        self.label_29.setObjectName("label_29")
        self.gridLayout_7.addWidget(self.label_29, 0, 0, 1, 1)
        self.aclNameStdNameLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_8)
        self.aclNameStdNameLineEdit.setObjectName("aclNameStdNameLineEdit")
        self.gridLayout_7.addWidget(self.aclNameStdNameLineEdit, 0, 1, 1, 1)
        self.aclNameStdCurrentLineEdit = QtWidgets.QTextEdit(self.gridLayoutWidget_8)
        self.aclNameStdCurrentLineEdit.setObjectName("aclNameStdCurrentLineEdit")
        self.gridLayout_7.addWidget(self.aclNameStdCurrentLineEdit, 2, 1, 1, 1)
        self.label_30 = QtWidgets.QLabel(self.gridLayoutWidget_8)
        self.label_30.setObjectName("label_30")
        self.gridLayout_7.addWidget(self.label_30, 2, 0, 1, 1)
        self.aclNameStdDenyPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_8)
        self.aclNameStdDenyPushButton.setObjectName("aclNameStdDenyPushButton")
        self.gridLayout_7.addWidget(self.aclNameStdDenyPushButton, 9, 1, 1, 1)
        self.aclNameStdSourceMaskLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_8)
        self.aclNameStdSourceMaskLineEdit.setObjectName("aclNameStdSourceMaskLineEdit")
        self.gridLayout_7.addWidget(self.aclNameStdSourceMaskLineEdit, 8, 1, 1, 1)
        self.label_31 = QtWidgets.QLabel(self.gridLayoutWidget_8)
        self.label_31.setObjectName("label_31")
        self.gridLayout_7.addWidget(self.label_31, 7, 1, 1, 1)
        self.label_32 = QtWidgets.QLabel(self.gridLayoutWidget_8)
        self.label_32.setObjectName("label_32")
        self.gridLayout_7.addWidget(self.label_32, 7, 0, 1, 1)
        self.aclNameStdAddPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_8)
        self.aclNameStdAddPushButton.setObjectName("aclNameStdAddPushButton")
        self.gridLayout_7.addWidget(self.aclNameStdAddPushButton, 10, 0, 1, 1)
        self.toolBox_5.addItem(self.page_21, "")
        self.page_22 = QtWidgets.QWidget()
        self.page_22.setGeometry(QtCore.QRect(0, 0, 711, 277))
        self.page_22.setObjectName("page_22")
        self.gridLayoutWidget_9 = QtWidgets.QWidget(self.page_22)
        self.gridLayoutWidget_9.setGeometry(QtCore.QRect(0, 0, 711, 271))
        self.gridLayoutWidget_9.setObjectName("gridLayoutWidget_9")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.gridLayoutWidget_9)
        self.gridLayout_8.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.label_37 = QtWidgets.QLabel(self.gridLayoutWidget_9)
        self.label_37.setObjectName("label_37")
        self.gridLayout_8.addWidget(self.label_37, 7, 0, 1, 1)
        self.label_34 = QtWidgets.QLabel(self.gridLayoutWidget_9)
        self.label_34.setObjectName("label_34")
        self.gridLayout_8.addWidget(self.label_34, 9, 0, 1, 1)
        self.label_35 = QtWidgets.QLabel(self.gridLayoutWidget_9)
        self.label_35.setObjectName("label_35")
        self.gridLayout_8.addWidget(self.label_35, 0, 0, 1, 1)
        self.aclNameExtSourceMaskLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_9)
        self.aclNameExtSourceMaskLineEdit.setObjectName("aclNameExtSourceMaskLineEdit")
        self.gridLayout_8.addWidget(self.aclNameExtSourceMaskLineEdit, 8, 1, 1, 1)
        self.aclNameExtNameLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_9)
        self.aclNameExtNameLineEdit.setObjectName("aclNameExtNameLineEdit")
        self.gridLayout_8.addWidget(self.aclNameExtNameLineEdit, 0, 1, 1, 1)
        self.aclNameExtDenyPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_9)
        self.aclNameExtDenyPushButton.setObjectName("aclNameExtDenyPushButton")
        self.gridLayout_8.addWidget(self.aclNameExtDenyPushButton, 11, 1, 1, 1)
        self.label_33 = QtWidgets.QLabel(self.gridLayoutWidget_9)
        self.label_33.setObjectName("label_33")
        self.gridLayout_8.addWidget(self.label_33, 7, 1, 1, 1)
        self.label_36 = QtWidgets.QLabel(self.gridLayoutWidget_9)
        self.label_36.setObjectName("label_36")
        self.gridLayout_8.addWidget(self.label_36, 2, 0, 1, 1)
        self.label_38 = QtWidgets.QLabel(self.gridLayoutWidget_9)
        self.label_38.setObjectName("label_38")
        self.gridLayout_8.addWidget(self.label_38, 9, 1, 1, 1)
        self.aclNameExtSourceAddressLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_9)
        self.aclNameExtSourceAddressLineEdit.setObjectName("aclNameExtSourceAddressLineEdit")
        self.gridLayout_8.addWidget(self.aclNameExtSourceAddressLineEdit, 8, 0, 1, 1)
        self.aclNameExtDestinationMaskLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_9)
        self.aclNameExtDestinationMaskLineEdit.setObjectName("aclNameExtDestinationMaskLineEdit")
        self.gridLayout_8.addWidget(self.aclNameExtDestinationMaskLineEdit, 10, 1, 1, 1)
        self.aclNameExtDestinationAddressLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_9)
        self.aclNameExtDestinationAddressLineEdit.setObjectName("aclNameExtDestinationAddressLineEdit")
        self.gridLayout_8.addWidget(self.aclNameExtDestinationAddressLineEdit, 10, 0, 1, 1)
        self.aclNameExtPermitPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_9)
        self.aclNameExtPermitPushButton.setObjectName("aclNameExtPermitPushButton")
        self.gridLayout_8.addWidget(self.aclNameExtPermitPushButton, 11, 0, 1, 1)
        self.aclNameExtCurrentTextEdit = QtWidgets.QTextEdit(self.gridLayoutWidget_9)
        self.aclNameExtCurrentTextEdit.setObjectName("aclNameExtCurrentTextEdit")
        self.gridLayout_8.addWidget(self.aclNameExtCurrentTextEdit, 2, 1, 1, 1)
        self.aclNameExtSourceProtocolLineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_9)
        self.aclNameExtSourceProtocolLineEdit.setObjectName("aclNameExtSourcePortLineEdit")
        self.gridLayout_8.addWidget(self.aclNameExtSourceProtocolLineEdit, 8, 2, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.gridLayoutWidget_9)
        self.label_7.setObjectName("label_7")
        self.gridLayout_8.addWidget(self.label_7, 9, 2, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.gridLayoutWidget_9)
        self.label_9.setObjectName("label_9")
        self.gridLayout_8.addWidget(self.label_9, 7, 2, 1, 1)
        self.aclNameExtAddPushButton = QtWidgets.QPushButton(self.gridLayoutWidget_9)
        self.aclNameExtAddPushButton.setObjectName("aclNameExtAddPushButton")
        self.gridLayout_8.addWidget(self.aclNameExtAddPushButton, 11, 2, 1, 1)
        self.toolBox_5.addItem(self.page_22, "")
        self.toolBox_3.addItem(self.page_18, "")
        self.toolBox_2.addItem(self.page_12, "")
        self.page_13 = QtWidgets.QWidget()
        self.page_13.setGeometry(QtCore.QRect(0, 0, 731, 393))
        self.page_13.setObjectName("page_13")
        self.formLayoutWidget_2 = QtWidgets.QWidget(self.page_13)
        self.formLayoutWidget_2.setGeometry(QtCore.QRect(0, 0, 711, 361))
        self.formLayoutWidget_2.setObjectName("formLayoutWidget_2")
        self.formLayout_4 = QtWidgets.QFormLayout(self.formLayoutWidget_2)
        self.formLayout_4.setContentsMargins(0, 0, 0, 0)
        self.formLayout_4.setObjectName("formLayout_4")
        self.staticRoutingDestinationIPLabel = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.staticRoutingDestinationIPLabel.setObjectName("staticRoutingDestinationIPLabel")
        self.formLayout_4.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.staticRoutingDestinationIPLabel)
        self.staticRoutingDestinationIPLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget_2)
        self.staticRoutingDestinationIPLineEdit.setObjectName("staticRoutingDestinationIPLineEdit")
        self.formLayout_4.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.staticRoutingDestinationIPLineEdit)
        self.staticRoutingMaskLabel = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.staticRoutingMaskLabel.setObjectName("staticRoutingMaskLabel")
        self.formLayout_4.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.staticRoutingMaskLabel)
        self.staticRoutingMaskLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget_2)
        self.staticRoutingMaskLineEdit.setObjectName("staticRoutingMaskLineEdit")
        self.formLayout_4.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.staticRoutingMaskLineEdit)
        self.staticRoutingGatewayLabel = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.staticRoutingGatewayLabel.setObjectName("staticRoutingGatewayLabel")
        self.formLayout_4.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.staticRoutingGatewayLabel)
        self.staticRoutingGatewayLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget_2)
        self.staticRoutingGatewayLineEdit.setObjectName("staticRoutingGatewayLineEdit")
        self.formLayout_4.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.staticRoutingGatewayLineEdit)
        self.staticRoutingAddPushButton = QtWidgets.QPushButton(self.formLayoutWidget_2)
        self.staticRoutingAddPushButton.setObjectName("staticRoutingAddPushButton")
        self.formLayout_4.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.staticRoutingAddPushButton)
        self.staticRoutingRemovePushButton = QtWidgets.QPushButton(self.formLayoutWidget_2)
        self.staticRoutingRemovePushButton.setObjectName("staticRoutingRemovePushButton")
        self.formLayout_4.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.staticRoutingRemovePushButton)
        self.toolBox_2.addItem(self.page_13, "")
        self.page_14 = QtWidgets.QWidget()
        self.page_14.setGeometry(QtCore.QRect(0, 0, 731, 393))
        self.page_14.setObjectName("page_14")
        self.formLayoutWidget_3 = QtWidgets.QWidget(self.page_14)
        self.formLayoutWidget_3.setGeometry(QtCore.QRect(9, 9, 711, 351))
        self.formLayoutWidget_3.setObjectName("formLayoutWidget_3")
        self.formLayout_5 = QtWidgets.QFormLayout(self.formLayoutWidget_3)
        self.formLayout_5.setContentsMargins(0, 0, 0, 0)
        self.formLayout_5.setObjectName("formLayout_5")
        self.ripNetworkAddressLabel = QtWidgets.QLabel(self.formLayoutWidget_3)
        self.ripNetworkAddressLabel.setObjectName("ripNetworkAddressLabel")
        self.formLayout_5.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ripNetworkAddressLabel)
        self.ripRoutingNetworkAddressLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget_3)
        self.ripRoutingNetworkAddressLineEdit.setObjectName("ripRoutingNetworkAddressLineEdit")
        self.formLayout_5.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ripRoutingNetworkAddressLineEdit)
        self.ripRoutingAddPushButton = QtWidgets.QPushButton(self.formLayoutWidget_3)
        self.ripRoutingAddPushButton.setObjectName("ripRoutingAddPushButton")
        self.formLayout_5.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ripRoutingAddPushButton)
        self.ripRoutingRemovePushButton = QtWidgets.QPushButton(self.formLayoutWidget_3)
        self.ripRoutingRemovePushButton.setObjectName("ripRoutingRemovePushButton")
        self.formLayout_5.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ripRoutingRemovePushButton)
        self.passiveInterfacesLabel = QtWidgets.QLabel(self.formLayoutWidget_3)
        self.passiveInterfacesLabel.setObjectName("passiveInterfacesLabel")
        self.formLayout_5.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.passiveInterfacesLabel)
        self.listWidget = QtWidgets.QListWidget(self.formLayoutWidget_3)
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.listWidget.setObjectName("listWidget")
        self.formLayout_5.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.listWidget)
        self.ripRoutingSelectPushButton = QtWidgets.QPushButton(self.formLayoutWidget_3)
        self.ripRoutingSelectPushButton.setObjectName("ripRoutingSelectPushButton")
        self.formLayout_5.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ripRoutingSelectPushButton)
        self.toolBox_2.addItem(self.page_14, "")
        self.page_15 = QtWidgets.QWidget()
        self.page_15.setGeometry(QtCore.QRect(0, 0, 731, 393))
        self.page_15.setObjectName("page_15")
        self.formLayoutWidget_4 = QtWidgets.QWidget(self.page_15)
        self.formLayoutWidget_4.setGeometry(QtCore.QRect(9, 9, 711, 351))
        self.formLayoutWidget_4.setObjectName("formLayoutWidget_4")
        self.formLayout_6 = QtWidgets.QFormLayout(self.formLayoutWidget_4)
        self.formLayout_6.setContentsMargins(0, 0, 0, 0)
        self.formLayout_6.setObjectName("formLayout_6")
        self.processNumberLabel = QtWidgets.QLabel(self.formLayoutWidget_4)
        self.processNumberLabel.setObjectName("processNumberLabel")
        self.formLayout_6.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.processNumberLabel)
        self.EIGRPRoutingProcessNumberLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget_4)
        self.EIGRPRoutingProcessNumberLineEdit.setObjectName("EIGRPRoutingProcessNumberLineEdit")
        self.formLayout_6.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.EIGRPRoutingProcessNumberLineEdit)
        self.networkAddressLabel = QtWidgets.QLabel(self.formLayoutWidget_4)
        self.networkAddressLabel.setObjectName("networkAddressLabel")
        self.formLayout_6.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.networkAddressLabel)
        self.EIGRPRoutingNetworkAddressLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget_4)
        self.EIGRPRoutingNetworkAddressLineEdit.setObjectName("EIGRPRoutingNetworkAddressLineEdit")
        self.formLayout_6.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.EIGRPRoutingNetworkAddressLineEdit)
        self.maskLabel = QtWidgets.QLabel(self.formLayoutWidget_4)
        self.maskLabel.setObjectName("maskLabel")
        self.formLayout_6.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.maskLabel)
        self.EIGRPRoutingMaskLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget_4)
        self.EIGRPRoutingMaskLineEdit.setObjectName("EIGRPRoutingMaskLineEdit")
        self.formLayout_6.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.EIGRPRoutingMaskLineEdit)
        self.checkBox = QtWidgets.QCheckBox(self.formLayoutWidget_4)
        self.checkBox.setObjectName("checkBox")
        self.formLayout_6.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.checkBox)
        self.EIGRPRoutingAddNetworkPushButton = QtWidgets.QPushButton(self.formLayoutWidget_4)
        self.EIGRPRoutingAddNetworkPushButton.setObjectName("EIGRPRoutingAddNetworkPushButton")
        self.formLayout_6.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.EIGRPRoutingAddNetworkPushButton)
        self.EIGRPRoutingRemoveNetworkPushButton = QtWidgets.QPushButton(self.formLayoutWidget_4)
        self.EIGRPRoutingRemoveNetworkPushButton.setObjectName("EIGRPRoutingRemoveNetworkPushButton")
        self.formLayout_6.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.EIGRPRoutingRemoveNetworkPushButton)
        self.toolBox_2.addItem(self.page_15, "")
        self.tabWidget.addTab(self.tab_4, "")

        self.retranslateUi(ripRoutingPassiveInterfacesListWidget)
        self.bindUI()
        self.tabWidget.setCurrentIndex(3)
        self.toolBox.setCurrentIndex(4)
        self.interfaceToolBox.setCurrentIndex(0)
        self.toolBox_2.setCurrentIndex(0)
        self.toolBox_3.setCurrentIndex(1)
        self.toolBox_4.setCurrentIndex(1)
        self.toolBox_5.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(ripRoutingPassiveInterfacesListWidget)

    def retranslateUi(self, ripRoutingPassiveInterfacesListWidget):
        _translate = QtCore.QCoreApplication.translate
        ripRoutingPassiveInterfacesListWidget.setWindowTitle(
            _translate("ripRoutingPassiveInterfacesListWidget", "Adcisco"))
        ripRoutingPassiveInterfacesListWidget.setWindowIcon(QtGui.QIcon('img/icon.jpg'))

        self.IPv4radioButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "IPv4"))
        self.IPv6radioButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "IPv6"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page), _translate("ripRoutingPassiveInterfacesListWidget",
                                                                             "show running-config"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page_3), _translate("ripRoutingPassiveInterfacesListWidget",
                                                                               "show int brief"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page_2), _translate("ripRoutingPassiveInterfacesListWidget",
                                                                               "show interfaces"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page_4), _translate("ripRoutingPassiveInterfacesListWidget",
                                                                               "show acess-lists"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page_5), _translate("ripRoutingPassiveInterfacesListWidget",
                                                                               "show ip route"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("ripRoutingPassiveInterfacesListWidget",
                                                                               "View configs"))
        self.addLoopbuttn.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Add Loopback"))
        self.duplexLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Duplex"))
        self.halfduplexRadio.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Half-duplex"))
        self.autoDuplexRadio.setText(_translate("ripRoutingPassiveInterfacesListWidget", "auto"))
        self.portLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Port status"))
        self.portStatus.setText(_translate("ripRoutingPassiveInterfacesListWidget", "On"))
        self.fullDuplexRadio.setText(_translate("ripRoutingPassiveInterfacesListWidget", "full-duplex"))
        self.groupBox_5.setTitle(_translate("ripRoutingPassiveInterfacesListWidget", "IPv4"))
        self.label_8.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Address"))
        self.label_5.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Subnet Mask"))
        self.groupBox_6.setTitle(_translate("ripRoutingPassiveInterfacesListWidget", "IPv6"))
        self.label_6.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Subnet Mask"))
        self.label_12.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Address"))
        self.interfaceToolBox.setItemText(self.interfaceToolBox.indexOf(self.page_9),
                                          _translate("ripRoutingPassiveInterfacesListWidget", "Address configuration"))
        self.label.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Subinterface number"))
        self.label_11.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Vlan Identifier"))
        self.label_13.setText(_translate("ripRoutingPassiveInterfacesListWidget", "IP Adress"))
        self.label_18.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Mask"))
        self.interfaceToolBox.setItemText(self.interfaceToolBox.indexOf(self.page_10),
                                          _translate("ripRoutingPassiveInterfacesListWidget", "Subinterfaces"))
        self.label_39.setText(_translate("ripRoutingPassiveInterfacesListWidget", "IN:"))
        self.label_40.setText(_translate("ripRoutingPassiveInterfacesListWidget", "OUT:"))
        self.pushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "OK"))
        self.interfaceToolBox.setItemText(self.interfaceToolBox.indexOf(self.page_11),
                                          _translate("ripRoutingPassiveInterfacesListWidget", "Access list"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3),
                                  _translate("ripRoutingPassiveInterfacesListWidget", "conf interface"))
        self.groupBox.setTitle(_translate("ripRoutingPassiveInterfacesListWidget", "Global config"))
        self.hostnameLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "hostname"))
        self.secretLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "secret password"))
        self.bannerMotdLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "banner motd"))
        self.domainlookupCheckBox.setText(_translate("ripRoutingPassiveInterfacesListWidget", "ip domain-lookup"))
        self.saveRunPushFileButton.setText(
            _translate("ripRoutingPassiveInterfacesListWidget", "Save running configuration to file"))
        self.loadRunPushButton_2.setText(
            _translate("ripRoutingPassiveInterfacesListWidget", "Load running configuration from file"))
        self.saveRunPushButton.setText(
            _translate("ripRoutingPassiveInterfacesListWidget", "Save Running Configuration"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2),
                                  _translate("ripRoutingPassiveInterfacesListWidget", "Setting"))
        self.aclNumStdPermitPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Permit"))
        self.label_22.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Number"))
        self.label_4.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Current Rules"))
        self.aclNumStdDenyPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Deny"))
        self.label_17.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source Mask"))
        self.label_15.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source address"))
        self.aclNumStdAddPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Add"))
        self.toolBox_4.setItemText(self.toolBox_4.indexOf(self.page_19),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "Standard"))
        self.label_24.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Current Rules"))
        self.label_27.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Destination address"))
        self.aclNumExtPermitPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Permit"))
        self.label_28.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Destination Mask"))
        self.label_25.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source Mask"))
        self.label_26.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source address"))
        self.aclNumExtDenyPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Deny"))
        self.label_23.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Number"))
        self.label_2.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Protocol"))
        self.aclNumExtAddPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Add"))
        self.toolBox_4.setItemText(self.toolBox_4.indexOf(self.page_20),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "Extended"))
        self.toolBox_3.setItemText(self.toolBox_3.indexOf(self.page_17),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "Numbered"))
        self.aclNameStdPermitPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Permit"))
        self.label_29.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Name"))
        self.label_30.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Current Rules"))
        self.aclNameStdDenyPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Deny"))
        self.label_31.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source Mask"))
        self.label_32.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source address"))
        self.aclNameStdAddPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Add"))
        self.toolBox_5.setItemText(self.toolBox_5.indexOf(self.page_21),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "Standard"))
        self.label_37.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source address"))
        self.label_34.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Destination address"))
        self.label_35.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Name"))
        self.aclNameExtDenyPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Deny"))
        self.label_33.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source Mask"))
        self.label_36.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Current Rules"))
        self.label_38.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Destination Mask"))
        self.aclNameExtPermitPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Permit"))
        self.label_7.setText(_translate("ripRoutingPassiveInterfacesListWidget", ""))
        self.label_9.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Source protocol"))
        self.aclNameExtAddPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Add"))
        self.toolBox_5.setItemText(self.toolBox_5.indexOf(self.page_22),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "Extended"))
        self.toolBox_3.setItemText(self.toolBox_3.indexOf(self.page_18),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "Named"))
        self.toolBox_2.setItemText(self.toolBox_2.indexOf(self.page_12),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "Access Lists"))
        self.staticRoutingDestinationIPLabel.setText(
            _translate("ripRoutingPassiveInterfacesListWidget", "Destination IP"))
        self.staticRoutingMaskLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Mask"))
        self.staticRoutingGatewayLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Gateway"))
        self.staticRoutingAddPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Add"))
        self.staticRoutingRemovePushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Remove"))
        self.toolBox_2.setItemText(self.toolBox_2.indexOf(self.page_13),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "Static Routing"))
        self.ripNetworkAddressLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Network Address"))
        self.ripRoutingAddPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Add"))
        self.ripRoutingRemovePushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Remove"))
        self.passiveInterfacesLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Passive interfaces"))
        self.ripRoutingSelectPushButton.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Select"))
        self.toolBox_2.setItemText(self.toolBox_2.indexOf(self.page_14),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "RIPv2 Routing"))
        self.processNumberLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Process Number"))
        self.networkAddressLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Network Address"))
        self.maskLabel.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Mask"))
        self.checkBox.setText(_translate("ripRoutingPassiveInterfacesListWidget", "Auto-summary network addresses"))
        self.EIGRPRoutingAddNetworkPushButton.setText(
            _translate("ripRoutingPassiveInterfacesListWidget", "Add Network"))
        self.EIGRPRoutingRemoveNetworkPushButton.setText(
            _translate("ripRoutingPassiveInterfacesListWidget", "Remove Network"))
        self.toolBox_2.setItemText(self.toolBox_2.indexOf(self.page_15),
                                   _translate("ripRoutingPassiveInterfacesListWidget", "EIGRP Routing"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4),
                                  _translate("ripRoutingPassiveInterfacesListWidget", "Security/Routing"))

    def bindUI(self):
        cisco1 = {
            "device_type": "cisco_ios",
            "host": "192.168.8.107",
            "username": "cisco",
            "password": "Cisco123",
            "secret": "class"
        }
        with open("log.dat", "rb") as f:
            a = pickle.load(f)
            f.close()
        self.nethandler = ConnectHandler(**a)
        out = os.remove("log.dat")
        #print(out)
        self.runConfTextEdit.showEvent = self.setCommand("show run", self.runConfTextEdit)
        self.interfaceBriefTextEdit.showEvent = self.showIPIntBr
        self.interfacesTextEdit.showEvent = self.setCommand("show interfaces", self.interfacesTextEdit)
        self.showACLTextEdit.showEvent = self.setCommand("show access-list", self.showACLTextEdit)
        self.ipRouteTextEdit.showEvent = self.setCommand("show ip route", self.ipRouteTextEdit)
        self.interfaceComboBox.showEvent = self.listinter

        # tab 2
        self.tab2 = Tab2(self.nethandler)
        self.portStatus.clicked.connect(lambda: self.tab2.setport(self.portStatus.isChecked(),
                                                             self.interfaceComboBox.currentText()))
        self.autoDuplexRadio.clicked.connect(self.switch_duplex)
        self.fullDuplexRadio.clicked.connect(self.switch_duplex)
        self.halfduplexRadio.clicked.connect(self.switch_duplex)
        self.addLoopbuttn.clicked.connect(self.tab2.add_loopback)
        self.intIPv4buttonBox.accepted.connect(lambda: self.tab2.address_conf_ipv4(self.intIPv4Address.text(),
                                                                                   self.intIPv4SubnetMask.text(),
                                                                                   self.interfaceComboBox.currentText()))
        self.intIPv4buttonBox.rejected.connect(lambda: (self.intIPv4Address.clear(), self.intIPv4SubnetMask.clear()))
        self.intIPv6buttonBox.accepted.connect(lambda: self.tab2.address_conf_ipv6(self.intIPv6Address.text(),
                                                                                   self.intIPv6SubnetMask.text(),
                                                                                   self.interfaceComboBox.currentText()))
        self.intIPv6buttonBox.rejected.connect(lambda: (self.intIPv6Address.clear(), self.intIPv6SubnetMask.clear()))
        self.configurePushButton_2.accepted.connect(lambda: self.tab2.subinterfaces(
            str(self.subinterfaceNumberLineEdit.text()), str(self.vlanID.text()),
            str(self.subinterfaceAddressLineEdit.text()), str(self.subinterfaceMaskLineEdit.text()),
            self.interfaceComboBox.currentText()))
        self.configurePushButton_2.rejected.connect(
            lambda: (self.subinterfaceNumberLineEdit.clear(), self.vlanID.clear(),
                     self.subinterfaceAddressLineEdit.clear(), self.subinterfaceMaskLineEdit.clear()))
        self.intAccLIN.showEvent = self.showACL
        self.pushButton.clicked.connect(self.AclSelect)

        # tab 3
        self.tab3 = Tab3(self.nethandler)
        data_router = self.read_data_roter()
        self.current_config(data_router)
        self.configurePushButton.accepted.connect(lambda: self.config_global(data_router))
        self.configurePushButton.rejected.connect(lambda: (self.hostnameLineEdit.clear(), self.secretLineEdit.clear(),
                                                           self.bannerMotdLineEdit.clear()))
        self.saveRunPushButton.clicked.connect(self.tab3.saveconfig)
        self.saveRunPushFileButton.clicked.connect(self.tab3.save_config_to_file)
        self.loadRunPushButton_2.clicked.connect(self.tab3.read_config_from_file)

        # zakladka 4
        self.tab4 = Tab4(self.nethandler)
        self.current_access_list = []
        self.staticRoutingAddPushButton.clicked.connect(lambda: self.tab4.add_static_routing(
            self.staticRoutingDestinationIPLineEdit.text(), self.staticRoutingMaskLineEdit.text(),
            self.staticRoutingGatewayLineEdit.text()))
        self.staticRoutingRemovePushButton.clicked.connect(lambda: self.tab4.delete_static_routing(
            self.staticRoutingDestinationIPLineEdit.text(), self.staticRoutingMaskLineEdit.text(),
            self.staticRoutingGatewayLineEdit.text()
        ))
        self.ripRoutingAddPushButton.clicked.connect(lambda: self.tab4.add_ripv2(
            self.ripRoutingNetworkAddressLineEdit.text()))
        self.ripRoutingRemovePushButton.clicked.connect(lambda: self.tab4.delete_ripv2(
            self.ripRoutingNetworkAddressLineEdit.text()))
        self.ripRoutingSelectPushButton.clicked.connect(self.passive_interface_ripv2)
        self.listWidget.itemSelectionChanged.connect(self.on_change)
        self.listWidget.showEvent = self.add_interfaces_to_listwidget
        self.EIGRPRoutingAddNetworkPushButton.clicked.connect(lambda: self.tab4.add_nertwork_eigrp(
            self.EIGRPRoutingProcessNumberLineEdit.text(), self.EIGRPRoutingNetworkAddressLineEdit.text(),
            self.EIGRPRoutingMaskLineEdit.text()))
        self.EIGRPRoutingRemoveNetworkPushButton.clicked.connect(lambda: self.tab4.delete_network_eigrp(
            self.EIGRPRoutingProcessNumberLineEdit.text(), self.EIGRPRoutingNetworkAddressLineEdit.text(),
            self.EIGRPRoutingMaskLineEdit.text()))
        self.checkBox.clicked.connect(lambda: self.tab4.auto_summary_eigrp(self.EIGRPRoutingProcessNumberLineEdit.text()
                                      , self.checkBox.isChecked()))
        self.aclNumStdPermitPushButton.clicked.connect(self.aclNumStdPermit)
        self.aclNumStdDenyPushButton.clicked.connect(self.aclNumStdDeny)
        self.aclNumStdAddPushButton.clicked.connect(self.aclNumStdAdd)
        self.aclNumExtPermitPushButton.clicked.connect(self.aclNumExtPermit)
        self.aclNumExtDenyPushButton.clicked.connect(self.aclNumExtDeny)
        self.aclNumExtAddPushButton.clicked.connect(self.aclNumExtAdd)
        self.aclNameStdPermitPushButton.clicked.connect(self.aclNameStdPermit)
        self.aclNameStdDenyPushButton.clicked.connect(self.aclNameStdDeny)
        self.aclNameStdAddPushButton.clicked.connect(self.aclNameStdAdd)
        self.aclNameExtPermitPushButton.clicked.connect(self.aclNameExtPermit)
        self.aclNameExtDenyPushButton.clicked.connect(self.aclNameExtDeny)
        self.aclNameExtAddPushButton.clicked.connect(self.aclNameExtAdd)

    def setCommand(self, command, prop):
        def show(event):
            prop.clear()
            self.nethandler.enable()
            output = self.nethandler.send_command(command)
            prop.append(output)

        return show

    def turnInterface(self, state):
        configSet = ["int " + self.interfaceComboBox.currentText()]
        if state == QtCore.Qt.Checked:
            configSet.append("no shutdown")
        else:
            configSet.append("shutdown")
        print(configSet)
        output = self.nethandler.send_config_set(configSet)
        print(output)

    def showACL(self, event):
        self.nethandler.enable()
        out = self.nethandler.send_command("show access-list").split("\n")
        aclList = []
        for item in out:
            if item.startswith((" ", "\t")):
                pass
            else:
                aclList.append(item.removeprefix("Standard ").removeprefix("Extended ").removeprefix("IP access list "))

        self.intAccLIN.clear()
        self.intAccLOUT.clear()
        self.intAccLIN.addItem("None")
        self.intAccLOUT.addItem("None")
        for item in aclList:
            self.intAccLIN.addItem(item)
            self.intAccLOUT.addItem(item)

    def AclSelect(self, event):
        aclIN = self.intAccLIN.currentText()
        aclOUT = self.intAccLOUT.currentText()
        if aclIN != "None":
            command = [
                "int " + self.interfaceComboBox.currentText(),
                "ip access-group " + aclIN + " in"
            ]
            out = self.nethandler.send_config_set(command)
            print(out)
        else:
            command = [
                "int " + self.interfaceComboBox.currentText(),
                "no ip access-group in"
            ]
            out = self.nethandler.send_config_set(command)
            print(out)
        if aclOUT != "None":
            command = [
                "int " + self.interfaceComboBox.currentText(),
                "ip access-group " + aclOUT + " out"
            ]
            out = self.nethandler.send_config_set(command)
            print(out)
        else:
            command = [
                "int " + self.interfaceComboBox.currentText(),
                "no ip access-group out"
            ]
            out = self.nethandler.send_config_set(command)
            print(out)

    def showIPIntBr(self, event):
        if self.IPv6radioButton.isChecked():
            command = "show ipv6 int brief"
        else:
            command = "show ip int brief"
        self.nethandler.enable()
        output = self.nethandler.send_command(command)
        self.interfaceBriefTextEdit.clear()
        self.interfaceBriefTextEdit.append(output)
        self.interfaceBriefTextEdit.resize(self.interfaceBriefTextEdit.maximumHeight(), self.toolBox.maximumWidth())
        # print(output)

    def listinter(self, event):
        command = "show ip interface brief"
        f = open("intTemplate.template", "r")
        self.nethandler.enable()
        output = self.nethandler.send_command(command)
        re_table = textfsm.TextFSM(f)
        header = re_table.header
        result = re_table.ParseText(output)
        self.interfaceComboBox.clear()
        for interface in result:
            self.interfaceComboBox.addItem(interface[0])
        self.interfaceComboBox.currentIndexChanged.connect(self.current_status)

    def current_status(self):
        self.intIPv4Address.clear()
        self.intIPv4SubnetMask.clear()
        self.intIPv6Address.clear()
        self.intIPv6SubnetMask.clear()
        command = "show running-config | section {}".format(self.interfaceComboBox.currentText())
        self.nethandler.enable()
        output = self.nethandler.send_command(command)
        if "shutdown" in output.split():
            self.portStatus.setChecked(False)
        else:
            self.portStatus.setChecked(True)

        if "duplex auto" in output.split():
            self.autoDuplexRadio.setChecked(True)
        elif "duplex half" in output.split():
            self.halfduplexRadio.setChecked(True)
        else:
            self.fullDuplexRadio.setChecked(True)
        if "no ip address" in output:
            self.intIPv4Address.clear()
            self.intIPv4SubnetMask.clear()
            self.intIPv6Address.clear()
            self.intIPv6SubnetMask.clear()
        else:
            if "ip" in output.split():
                ind = output.split().index('ip')
                self.intIPv4Address.insert(output.split()[ind + 2])
                self.intIPv4SubnetMask.insert(output.split()[ind + 3])
            if "ipv6" in output.split():
                ind = output.split().index('ipv6')
                self.intIPv6Address.insert(output.split()[ind + 2][:-3:])
                self.intIPv6SubnetMask.insert(output.split()[ind + 2][-3::])

    def update(self):
        command = "show ip interface brief"
        f = open("intTemplate.template", "r")
        self.nethandler.enable()
        output = self.nethandler.send_command(command)
        re_table = textfsm.TextFSM(f)
        header = re_table.header
        result = re_table.ParseText(output)
        for row in result:
            if self.interfaceComboBox.currentText() in row:
                if row[2] == "up":
                    self.portStatus.setChecked(True)
                else:
                    self.portStatus.setChecked(False)
                self.intIPv4Address.clear()
                self.intIPv4Address.insert(row[1])

    def switch_duplex(self):
        if self.halfduplexRadio.isChecked():
            command = [
                f"int {self.interfaceComboBox.currentText()}",
                "duplex half"
            ]
            self.nethandler.send_config_set(command)

        if self.fullDuplexRadio.isChecked():
            command = [
                "int {}".format(self.interfaceComboBox.currentText()),
                "duplex full"
            ]
            self.nethandler.send_config_set(command)

        if self.autoDuplexRadio.isChecked():
            command = [
                "int {}".format(self.interfaceComboBox.currentText()),
                "duplex auto"
            ]
            self.nethandler.send_config_set(command)

    # zakladka 3
    def read_data_roter(self):
        lista = list()
        comm = "show running-config | include host"
        self.nethandler.enable()
        output = self.nethandler.send_command(comm)
        lista.append(output.strip('\n').replace("hostname ", ""))
        comm2 = "show running-config | include enable secret "
        output2 = self.nethandler.send_command(comm2)
        if str(output2):
            lista.append("password encryption")
        else:
            lista.append("empty")
        comm3 = "show running-config | include banner"
        output3 = self.nethandler.send_command(comm3)
        if str(output3):
            test = (output3.rstrip('^C').replace("banner motd ", "").strip('^C'))
            lista.append(test.replace('^C', ""))
        else:
            lista.append("empty")
        comm4 = "show running-config | include no ip domain lookup"
        output4 = self.nethandler.send_command(comm4)
        if str(output4):
            lista.append("no")
        else:
            lista.append("ip")
        return lista

    def current_config(self, data_router):
        self.hostnameLineEdit.setText(data_router[0])
        if "empty" not in data_router[1]:
            self.secretLineEdit.setText(data_router[1])
        if "empty" not in data_router[2]:
            self.bannerMotdLineEdit.setText(data_router[2])
        if "ip" in data_router[3]:
            self.domainlookupCheckBox.setChecked(True)
        else:
            self.domainlookupCheckBox.setChecked(False)

    def config_global(self, data_router):
        def setname(name):
            if name in data_router[0]:
                self.window_messages("The same hostname was entered")
            else:
                command = "host {}".format(name)
                self.nethandler.enable()
                self.nethandler.send_config_set(command)

        def setpassword(passw):
            if passw in data_router[1]:
                self.window_messages("The same password was entered")
            else:
                command = "enable secret {}".format(passw)
                self.nethandler.send_config_set(command)

        def setbanner(banner):
            if banner in data_router[2]:
                self.window_messages("The same banner motd was entered")
            else:
                command = "banner motd #{}#".format(banner)
                self.nethandler.send_config_set(command)

        def switch_domain():
            if self.domainlookupCheckBox.isChecked():
                comm = "ip domain-lookup"
            else:
                comm = "no ip domain-lookup"
            self.nethandler.send_config_set(comm)

        if str(self.hostnameLineEdit.text()):
            setname(str(self.hostnameLineEdit.text()))
        if str(self.secretLineEdit.text()):
            setpassword(str(self.secretLineEdit.text()))
        if str(self.bannerMotdLineEdit.text()):
            setbanner(str(self.bannerMotdLineEdit.text()))
        switch_domain()

    # tab 4 # passive

    def passive_interface_ripv2(self):

        def check_passve_interface():
            command = "show running-config | section passive"
            self.nethandler.enable()
            output = self.nethandler.send_command(command)
            list_interfaces = self.exist_interfaces()
            list_passive = [i for i in output.split() if i.startswith("Gig") or
                            i.startswith("Fa") or i.startswith("Loop") or i.startswith("serial")]
            list_no_passive = list(set(list_interfaces) - set(list_passive))
            return list_passive, list_no_passive

        choosed_interfaces = self.on_change()
        if choosed_interfaces:
            list_passive, list_no_passive = check_passve_interface()
            for interfaces in choosed_interfaces:
                if interfaces in list_passive:
                    command = [
                        "router rip",
                        "rip version 2",
                        "no passive-interface {}".format(interfaces)
                    ]
                    self.nethandler.send_config_set(command)
                elif interfaces in list_no_passive:
                    command = [
                        "router rip",
                        "rip version 2",
                        "passive-interface {}".format(interfaces)
                    ]
                    self.nethandler.send_config_set(command)
        else:
            self.window_messages("No interface was selected")

    def exist_interfaces(self):
        comm = "show ip int brief"
        self.nethandler.enable()
        ot = self.nethandler.send_command(comm)
        list_interfaces = [i for i in ot.split() if
                           i.startswith("Gig") or i.startswith("Fa") or i.startswith("Loop") or i.startswith("serial")]
        return list_interfaces

    def add_interfaces_to_listwidget(self, event):
        list_interfaces = self.exist_interfaces()
        self.listWidget.clear()
        for z in list_interfaces:
            self.listWidget.addItem(z)

    def on_change(self):
        choosed_interfaces = [item.text() for item in self.listWidget.selectedItems()]
        return choosed_interfaces

    def aclNumStdPermit(self, event):
        sourceAddress = self.aclNumStdSourceAddressLineEdit.text()
        sourceMask = self.aclNumStdSourceMaskLineEdit.text()
        command = "permit " + str(sourceAddress) + " " + str(sourceMask)
        self.current_access_list.append(command)
        self.aclNumStdCurrentLineEdit.clear()
        for item in self.current_access_list:
            self.aclNumStdCurrentLineEdit.insertPlainText(item + "\n")

    def aclNumStdDeny(self, event):
        sourceAddress = self.aclNumStdSourceAddressLineEdit.text()
        sourceMask = self.aclNumStdSourceMaskLineEdit.text()
        command = "deny " + str(sourceAddress) + " " + str(sourceMask)
        self.current_access_list.append(command)
        self.aclNumStdCurrentLineEdit.clear()
        for item in self.current_access_list:
            self.aclNumStdCurrentLineEdit.insertPlainText(item + "\n")

    def aclNumStdAdd(self, event):
        command = "ip access-list standard " + self.aclNumStdNumLineEdit.text()
        self.current_access_list.insert(0, command)
        output = self.nethandler.send_config_set(self.current_access_list)
        self.current_access_list.clear()
        self.aclNumStdCurrentLineEdit.clear()
        self.aclNumStdSourceAddressLineEdit.clear()
        self.aclNumStdSourceMaskLineEdit.clear()
        self.aclNumStdNumLineEdit.clear()

    def aclNumExtPermit(self, event):
        sourceAddress = self.aclNumExtSourceAddressLineEdit.text()
        sourceMask = self.aclNumExtSourceMaskLineEdit.text()
        sourceProtocol = self.aclNumExtSourceProtocolLineEdit.text()
        destinationAddress = self.aclNumExtDestinationAddressLineEdit.text()
        destinationMask = self.aclNumExtDestinationMaskLineEdit.text()
        command = " ".join(('permit', str(sourceProtocol), str(sourceAddress), str(sourceMask), destinationAddress, destinationMask))
        self.current_access_list.append(str(command))
        self.aclNumExtCurrentTextEdit.clear()
        for item in self.current_access_list:
            self.aclNumExtCurrentTextEdit.insertPlainText(item + "\n")

    def aclNumExtDeny(self, event):
        sourceAddress = self.aclNumExtSourceAddressLineEdit.text()
        sourceMask = self.aclNumExtSourceMaskLineEdit.text()
        sourceProtocol = self.aclNumExtSourceProtocolLineEdit.text()
        destinationAddress = self.aclNumExtDestinationAddressLineEdit.text()
        destinationMask = self.aclNumExtDestinationMaskLineEdit.text()
        command = "deny " + sourceProtocol + " " + str(sourceAddress) + " " + str(
            sourceMask) + " " + destinationAddress + " " + destinationMask
        self.current_access_list.append(command)
        self.aclNumExtCurrentTextEdit.clear()
        for item in self.current_access_list:
            self.aclNumExtCurrentTextEdit.insertPlainText(item + "\n")

    def aclNumExtAdd(self, event):
        command = "ip access-list extended " + self.aclNumExtNumLineEdit.text()
        self.current_access_list.insert(0, command)
        output = self.nethandler.send_config_set(self.current_access_list)
        self.current_access_list.clear()
        self.aclNumExtCurrentTextEdit.clear()
        self.aclNumExtSourceAddressLineEdit.clear()
        self.aclNumExtSourceMaskLineEdit.clear()
        self.aclNumExtSourceProtocolLineEdit.clear()
        self.aclNumExtDestinationAddressLineEdit.clear()
        self.aclNumExtDestinationMaskLineEdit.clear()
        self.aclNumExtNumLineEdit.clear()

    def aclNameStdPermit(self, event):
        sourceAddress = self.aclNameStdSourceAddressLineEdit.text()
        sourceMask = self.aclNameStdSourceMaskLineEdit.text()
        command = "permit " + str(sourceAddress) + " " + str(sourceMask)
        self.current_access_list.append(command)
        self.aclNameStdCurrentLineEdit.clear()
        for item in self.current_access_list:
            self.aclNameStdCurrentLineEdit.insertPlainText(item + "\n")

    def aclNameStdDeny(self, event):
        sourceAddress = self.aclNameStdSourceAddressLineEdit.text()
        sourceMask = self.aclNameStdSourceMaskLineEdit.text()
        command = "deny " + str(sourceAddress) + " " + str(sourceMask)
        self.current_access_list.append(command)
        self.aclNameStdCurrentLineEdit.clear()
        for item in self.current_access_list:
            self.aclNameStdCurrentLineEdit.insertPlainText(item + "\n")

    def aclNameStdAdd(self, event):
        command = "ip access-list standard " + self.aclNameStdNameLineEdit.text()
        self.current_access_list.insert(0, command)
        output = self.nethandler.send_config_set(self.current_access_list)
        self.current_access_list.clear()
        self.aclNameStdCurrentLineEdit.clear()
        self.aclNameStdSourceAddressLineEdit.clear()
        self.aclNameStdSourceMaskLineEdit.clear()
        self.aclNameStdNameLineEdit.clear()

    def aclNameExtPermit(self, event):
        sourceAddress = self.aclNameExtSourceAddressLineEdit.text()
        sourceMask = self.aclNameExtSourceMaskLineEdit.text()
        sourceProtocol = self.aclNameExtSourceProtocolLineEdit.text()
        destinationAddress = self.aclNameExtDestinationAddressLineEdit.text()
        destinationMask = self.aclNameExtDestinationMaskLineEdit.text()
        command = " ".join(('permit', str(sourceProtocol), str(sourceAddress), str(sourceMask), destinationAddress, destinationMask))
        self.current_access_list.append(str(command))
        self.aclNameExtCurrentTextEdit.clear()
        for item in self.current_access_list:
            self.aclNameExtCurrentTextEdit.insertPlainText(item + "\n")

    def aclNameExtDeny(self, event):
        sourceAddress = self.aclNameExtSourceAddressLineEdit.text()
        sourceMask = self.aclNameExtSourceMaskLineEdit.text()
        sourceProtocol = self.aclNameExtSourceProtocolLineEdit.text()
        destinationAddress = self.aclNameExtDestinationAddressLineEdit.text()
        destinationMask = self.aclNameExtDestinationMaskLineEdit.text()
        command = " ".join(('deny', str(sourceProtocol), str(sourceAddress), str(sourceMask), destinationAddress, destinationMask))
        self.current_access_list.append(str(command))
        self.aclNameExtCurrentTextEdit.clear()
        for item in self.current_access_list:
            self.aclNameExtCurrentTextEdit.insertPlainText(item + "\n")

    def aclNameExtAdd(self, event):
        command = "ip access-list extended " + self.aclNameExtNameLineEdit.text()
        self.current_access_list.insert(0, command)
        output = self.nethandler.send_config_set(self.current_access_list)
        self.current_access_list.clear()
        self.aclNameExtCurrentTextEdit.clear()
        self.aclNameExtSourceAddressLineEdit.clear()
        self.aclNameExtSourceMaskLineEdit.clear()
        self.aclNameExtSourceProtocolLineEdit.clear()
        self.aclNameExtDestinationAddressLineEdit.clear()
        self.aclNameExtDestinationMaskLineEdit.clear()
        self.aclNameExtNameLineEdit.clear()

    def window_messages(self, message):
        self.Window_m = WindowMessage(message)
        self.Window_m.show()


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    ripRoutingPassiveInterfacesListWidget = QtWidgets.QWidget()
    ui = Ui_ripRoutingPassiveInterfacesListWidget()
    ui.setupUi(ripRoutingPassiveInterfacesListWidget)
    ripRoutingPassiveInterfacesListWidget.show()
    sys.exit(app.exec_())
