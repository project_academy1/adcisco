import pickle
import subprocess
from os import path
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QDesktopWidget
from netmiko.ssh_dispatcher import ConnectHandler
from windows import WindowMessage


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(753, 261)
        qr = MainWindow.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        MainWindow.move(qr.topLeft())
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.login_entry = QtWidgets.QLineEdit(self.centralwidget)
        self.login_entry.setGeometry(QtCore.QRect(490, 30, 141, 31))
        self.login_entry.setObjectName("login_entry")
        self.label_login = QtWidgets.QLabel(self.centralwidget)
        self.label_login.setGeometry(QtCore.QRect(410, 40, 67, 17))
        self.label_login.setObjectName("label_login")
        self.pass_entry = QtWidgets.QLineEdit(self.centralwidget)
        self.pass_entry.setGeometry(QtCore.QRect(490, 70, 141, 31))
        self.pass_entry.setObjectName("pass_entry")
        self.label_pass = QtWidgets.QLabel(self.centralwidget)
        self.label_pass.setGeometry(QtCore.QRect(410, 80, 67, 17))
        self.label_pass.setObjectName("label_pass")
        self.secret_entry = QtWidgets.QLineEdit(self.centralwidget)
        self.secret_entry.setGeometry(QtCore.QRect(490, 150, 141, 31))
        self.secret_entry.setObjectName("secret_entry")
        self.label_secret = QtWidgets.QLabel(self.centralwidget)
        self.label_secret.setGeometry(QtCore.QRect(410, 150, 67, 17))
        self.label_secret.setObjectName("label_secret")
        self.saveButon = QtWidgets.QPushButton(self.centralwidget)
        self.saveButon.setGeometry(QtCore.QRect(410, 200, 97, 33))
        self.saveButon.setObjectName("saveButon")
        self.logButton = QtWidgets.QPushButton(self.centralwidget)
        self.logButton.setGeometry(QtCore.QRect(510, 200, 97, 33))
        self.logButton.setObjectName("logButton")
        self.ip_entry = QtWidgets.QLineEdit(self.centralwidget)
        self.ip_entry.setGeometry(QtCore.QRect(490, 110, 141, 31))
        self.ip_entry.setObjectName("ip_entry")
        self.label_ip = QtWidgets.QLabel(self.centralwidget)
        self.label_ip.setGeometry(QtCore.QRect(410, 120, 81, 17))
        self.label_ip.setObjectName("label_ip")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(0, 0, 391, 241))
        self.listWidget.setObjectName("listWidget")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Adcisco Log IOS"))
        MainWindow.setWindowIcon(QtGui.QIcon('img/icon.jpg'))
        self.label_login.setText(_translate("MainWindow", "login"))
        self.label_pass.setText(_translate("MainWindow", "password"))
        self.label_secret.setText(_translate("MainWindow", "secret"))
        self.saveButon.setText(_translate("MainWindow", "Save entry"))
        self.logButton.setText(_translate("MainWindow", "Log"))
        self.label_ip.setText(_translate("MainWindow", "ip address"))
        self.saveButon.clicked.connect(self.save_entry)
        self.logButton.clicked.connect(self.log_cisco)
        self.listWidget.itemDoubleClicked.connect(self.read_entry)
        self.view_entry()

    def view_entry(self):
        if path.isfile("profile_log.txt"):
            with open("profile_log.txt", 'r+') as f:
                # pw =sha256(password.encode('utf-8'))
                for line in f.readlines():
                    self.listWidget.addItem(line)
        else:
            with open('profile_log.txt', 'w') as fp:
                pass

    def save_entry(self):
        ip = str(self.ip_entry.text())
        login = str(self.login_entry.text())
        password = str(self.pass_entry.text())
        secret = str(self.secret_entry.text())
        self.listWidget.clear()
        if not ip or not login or not password:
            self.window_messages("All fields must be completed")
        else:
            with open("profile_log.txt", 'a+') as f:
                # pw =sha256(password.encode('utf-8'))
                f.write("{0}:{1}:{2}:{3} \n".format(login, password, ip, secret))
        self.view_entry()

    def read_entry(self, item):
        field_content = item.text().split(":")
        login = field_content[0]
        password = field_content[1]
        ip = field_content[2]
        secret = field_content[3].strip(" \n")
        self.login_entry.setText(login)
        self.pass_entry.setText(password)
        self.ip_entry.setText(ip)
        self.secret_entry.setText(secret)

    def log_cisco(self):
        ip = str(self.ip_entry.text())
        login = str(self.login_entry.text())
        password = str(self.pass_entry.text())
        secret = str(self.secret_entry.text())
        if not ip or not login or not password or not secret:
            self.window_messages("All fields must be completed")
        else:
            logInfo = {
                "device_type": "cisco_ios",
                "host": ip,
                "username": login,
                "password": password,
                "secret": secret
            }
            nethandler = ConnectHandler(**logInfo)
            with open("log.dat", "wb") as f:
                pickle.dump(logInfo, f)
                f.close()
            MainWindow.close()
            subprocess.run(["python", "adcisco.py"])

    def window_messages(self, message):
        self.Window_m = WindowMessage(message)
        self.Window_m.show()


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
