from windows import WindowMessage


class Tab4:
    def __init__(self, nethandler):
        self.nethandler = nethandler

    def window_messages(self, message):
        self.Window_m = WindowMessage(message)
        self.Window_m.show()

    def add_static_routing(self, ip, mask, gateway):
        if ip and mask and gateway:
            command = "ip route {} {} {}".format(ip, mask, gateway)
            try:
                self.nethandler.send_config_set(command)
            except Exception as err:
                exception_type = type(err).__name__
                self.window_messages(f"The operation was unsuccessful: {exception_type}")
        else:
            self.window_messages("All fields must be completed")

    def delete_static_routing(self, ip, mask, gateway):
        if ip and mask and gateway:
            command = "no ip route {} {} {}".format(ip, mask, gateway)
            try:
                self.nethandler.send_config_set(command)
            except Exception as err:
                exception_type = type(err).__name__
                self.window_messages(f"The operation was unsuccessful: {exception_type}")
        else:
            self.window_messages("All fields must be completed")

    def add_ripv2(self, network_add):
        if network_add:
            command = [
                "router rip",
                "version 2",
                "network {}".format(network_add)
            ]
            self.nethandler.send_config_set(command)
        else:
            self.window_messages("Field must be completed")

    def delete_ripv2(self, network_add):
        if network_add:
            command = [
                "router rip",
                "version 2",
                "no network {}".format(network_add)
            ]
            self.nethandler.send_config_set(command)
        else:
            self.window_messages("Field must be completed")

    def add_nertwork_eigrp(self, process_number, network_add, mask):
        if network_add and process_number:
            command = [
                f"router eigrp {process_number}",
                "network {} {}".format(network_add, mask)
            ]
            self.nethandler.send_config_set(command)
        else:
            self.window_messages("All fields must be completed")

    def delete_network_eigrp(self, process_number, network_add, mask):
        if network_add and process_number:
            command = [
                f"router eigrp {process_number}",
                "no network {} {}".format(network_add, mask)
            ]
            self.nethandler.send_config_set(command)
        else:
            self.window_messages("All fields must be completed")

    def auto_summary_eigrp(self, process_number, ischecked):
        if process_number:
            if ischecked:
                command = [
                    f"router eigrp {process_number}",
                    "auto-summary"
                ]
                self.nethandler.send_config_set(command)
            else:
                command = [
                    f"router eigrp {process_number}",
                    "no auto-summary"
                ]
                self.nethandler.send_config_set(command)
        else:
            self.window_messages("Field process_number must be completed")