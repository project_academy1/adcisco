# adcisco

The Program is used to manage Cisco routers using the ssh protocol

![project](./img/log.png)

![project2](./img/config.png)

## Features

1. The Program view basic configuration interface and acl
2. interface configuration
3. Create interfaces loopback
4. Program ensures basic configration among others change username , password , bannder motd and ip domain lookup
5. List ACL and routing static  and dynamic(eigrp, ripv2) configuration
6. Login panel

## Installation

Installation requirement packages python

```sh
pip install -r requirements.txt
```

## Running

```python3
python3 main.py
```

## Tech

* python 3.9
* pyQt5
* netmiko

## Authors

*****
__goodbit22__ --> <https://gitlab.com/users/goodbit22>
*****
__hagaven__   --> <https://gitlab.com/hagaven>
*****
